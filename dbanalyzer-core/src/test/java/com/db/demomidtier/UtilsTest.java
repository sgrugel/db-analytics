package com.db.demomidtier;

import com.mockrunner.mock.jdbc.MockResultSet;
import org.json.JSONArray;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.sql.*;

import static com.db.demomidtier.Utils.convertResultSetToJsonArray;
import static com.db.demomidtier.Utils.convertResultSetToJsonString;
import static java.util.Collections.singletonList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class UtilsTest {

    @Mock
    private Connection jdbcConnection;

    @Mock
    private ResultSet resultSet;

    @Mock
    private Statement statement;

    @Mock
    private PreparedStatement preparedStatement;

    @Mock
    private ResultSetMetaData resultSetMetaData;

    private String query = "SELECT * FROM test";
    private String queryWithParam = "SELECT * FROM ?";
    private String queryDbName = "test";

    @Before
    public void setUp() throws Exception {
        resultSet = Mockito.mock(ResultSet.class);
        resultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString(1)).thenReturn("");
        Mockito.when(resultSet.getMetaData()).thenReturn(resultSetMetaData);
        Mockito.when(resultSet.getMetaData().getColumnCount()).thenReturn(1);
        Mockito.when(resultSet.getMetaData().getColumnLabel(1)).thenReturn("");

        statement = Mockito.mock(Statement.class);
        Mockito.when(statement.executeQuery(query)).thenReturn(resultSet);

        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);

        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.createStatement()).thenReturn(statement);
        Mockito.when(jdbcConnection.prepareStatement(queryWithParam)).thenReturn(preparedStatement);
        Mockito.when(jdbcConnection.prepareStatement(query)).thenReturn(preparedStatement);
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(true);
    }

    @Test
    public void test_connection(){
        assertThat(new Utils(jdbcConnection).isConnected(),is(true));
    }

    @Test
    public void test_execute_sql() throws SQLException {
        String data = new Utils(jdbcConnection).executeSQL(query).toString();
        assertThat(data,is("[{}]"));
    }

    @Test
    public void test_execute_sql_with_param() throws SQLException {
        String data = new Utils(jdbcConnection).executeSQL(queryWithParam,queryDbName).toString();
        assertThat(data,is("[{}]"));
    }

    @Test
    public void convert_null_to_string() throws Exception {
        assertThat(convertResultSetToJsonString(null),is("null"));
    }

    @Test
    public void convert_null_to_json_array() throws Exception {
        assertThat(convertResultSetToJsonArray(null),is((JSONArray) null));
    }

    @Test
    public void convert_empty_set_to_string() throws Exception{
        MockResultSet empty_set = new MockResultSet("empty_set");
        assertThat(convertResultSetToJsonString(empty_set),is("[]"));
    }

    @Test
    public void convert_simple_set_to_string() throws Exception{
        MockResultSet simple_set = new MockResultSet("simple_set");
        simple_set.addColumn("0");
        simple_set.addRow(singletonList("0"));
        assertThat(convertResultSetToJsonString(simple_set),is("[{\"0\":\"0\"}]"));
    }

    @Test
    public void test_creation(){
        Utils utils = new Utils(jdbcConnection);
        assertThat(utils.getDBAddress(),is("jdbc:mysql://10.0.75.1:3307/"));
    }

    @Test
    public void test_getter(){
        Utils utils = new Utils(jdbcConnection);
        utils.usePropertyFile("dbConnector.properties");
        assertThat(utils.getDBAddress(),is("jdbc:mysql://10.0.75.1:3307/"));
        assertThat(utils.getViewName(),is("db_grad_cs_1917.deal_view"));
    }

    @Test
    public void test_invalid_properties(){
        Utils utils = new Utils(jdbcConnection);
        assertNull(utils.getConnection("dbConnectorInvalid.properties"));
    }

    @Test
    public void test_without_connection() throws SQLException {
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.isValid(1)).thenReturn(false);
        Utils utils = new Utils(jdbcConnection);
        assertNull(utils.executeSQL(query));
    }

    @Test
    public void test_exception_during_execution() throws SQLException {
        Mockito.reset(preparedStatement);
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenThrow(new SQLException());
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.prepareStatement(query)).thenReturn(preparedStatement);

        Utils utils = new Utils(jdbcConnection);
        assertNull(utils.executeSQL(query));
    }

    @Test
    public void test_exception_in_resultset_in_meta() throws SQLException {
        Mockito.reset(resultSet);
        resultSet = Mockito.mock(ResultSet.class);
        Mockito.when(resultSet.getMetaData()).thenThrow(new SQLException());
        Mockito.reset(preparedStatement);
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.prepareStatement(query)).thenReturn(preparedStatement);

        Utils utils = new Utils(jdbcConnection);
        assertNull(utils.executeSQL(query));
    }

    @Test
    public void test_exception_in_resultset_in_meta_column() throws SQLException {
        Mockito.reset(resultSetMetaData);
        resultSetMetaData = Mockito.mock(ResultSetMetaData.class);
        Mockito.when(resultSetMetaData.getColumnCount()).thenThrow(new SQLException());
        Mockito.reset(resultSet);
        resultSet = Mockito.mock(ResultSet.class);
        Mockito.when(resultSet.getMetaData()).thenReturn(resultSetMetaData);
        Mockito.reset(preparedStatement);
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.prepareStatement(query)).thenReturn(preparedStatement);

        Utils utils = new Utils(jdbcConnection);
        assertNull(utils.executeSQL(query));
    }

    @Test
    public void test_null_resultset() throws SQLException {
        Mockito.reset(preparedStatement);
        preparedStatement = Mockito.mock(PreparedStatement.class);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(null);
        Mockito.reset(jdbcConnection);
        jdbcConnection = Mockito.mock(Connection.class);
        Mockito.when(jdbcConnection.prepareStatement(query)).thenReturn(preparedStatement);

        Utils utils = new Utils(jdbcConnection);
        assertNull(utils.executeSQL(query));
    }
}