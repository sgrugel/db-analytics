 $(document).ready(function() { 
  $('#instrumentDropDown').select();
  $.ajax({
  method: 'GET',
    url: "instrumentNameList.jsp",
    success: function(msg) {
      if(msg == "error") {
          console.log(msg);
      } else {
      var instrumentDropList = JSON.parse(msg);
      $.each(instrumentDropList, function(i) {
        $("#instrumentDropDown").append('<option value="'+this.instrument_name+'">'+this.instrument_name+'</option>');
      }); 
      
    }
  },
  error: function(msg) {
    console.log("didnt work");
  }
  });
     $('#instrumentDropDown').select2({
      placeholder: 'Select an Instrument!'
    }).on('change', function() {
      $("#instrumentsGraph").css("display", "block");
      var instrumentName = $("#instrumentDropDown option:selected").text();
      $.ajax({
        method: 'GET',
        url: "getInstrumentDetails.jsp",
        data: "instrument="+instrumentName,
        dataType: "json",
        success: function(msg) {
          if(msg == "error") {
          console.log(msg);
         } else {
          console.log(msg);
          var newMsg = [{name: "Buy", values: []}, {name: "Sell", values: []}]
          var buyValues = [];
          var sellValues = [];
          var buyTimes = [];
          var sellTimes = [];
          var buyTimes1 = [];
          var sellTimes1 = [];
          var buyIter = 0;
          var sellIter = 0;
          var buySum = 0;
          var sellSum = 0;
          var date;
          var price;
          for(var i = 0; i < msg.length; i++) {
            if(msg[i].buy != 0 && msg[i].sell == 0) {
              buyTimes1[buyIter] = new Date(msg[i].dealtime);
              buyTimes[buyIter] = buyIter;
              buyValues[buyIter] = msg[i].buy;
              buySum+=msg[i].buy;
              buyIter++;
            } else if(msg[i].sell != 0 && msg[i].buy == 0) {
              sellTimes1[sellIter] = new Date(msg[i].dealtime);
              sellTimes[sellIter] = sellIter;
              sellValues[sellIter] = msg[i].sell;
              sellSum+=msg[i].sell;
          
              sellIter++;
            } else {
              console.log("error");
            }
          }
          if(msg[msg.length-1].buy != 0 && msg[msg.length-1].sell == 0) {
            var closingPrice = msg[msg.length-1].buy;
          } else if(msg[msg.length-1].sell != 0 && msg[msg.length-1].buy == 0) {
            var closingPrice = msg[msg.length-1].sell;
          } else {
            console.log("error");
          }
           if(msg[0].buy != 0 && msg[0].sell == 0) {
            var startingPrice = msg[0].buy;
          } else if(msg[0].sell != 0 && msg[0].buy == 0) {
            var startingPrice = msg[0].sell;
          } else {
            console.log("error");
          }


          var averageBuy = buySum / msg.length;
          var averageSell = sellSum / msg.length;
          var percentChange = (closingPrice - startingPrice)/startingPrice;
          var maxPrice = Math.max(Math.max(...buyValues),Math.max(...sellValues));
          var minPrice = Math.min(Math.min(...buyValues),Math.min(...sellValues));
          var volatility = (maxPrice - minPrice)/minPrice;

          $('#openCard').html(startingPrice.toFixed(2));
          $('#closeCard').html(closingPrice.toFixed(2));
          $('#buyCard').html(averageBuy.toFixed(2));
          $('#sellCard').html(averageSell.toFixed(2));
          $('#highCard').html(maxPrice.toFixed(2));
          $('#lowCard').html(minPrice.toFixed(2));
          $('#percentChange').html((percentChange*100).toFixed(2) + "%");
          $('#volatility').html((volatility*100).toFixed(2) + "%");

                var trace1 = {
                type: "scatter",
                mode: "lines",
                name: 'Buy',
                x: buyTimes,
                y: buyValues,
                line: {color: '#0000FF'}
              }

              var trace2 = {
                type: "scatter",
                mode: "lines",
                name: 'Sell',
                x: sellTimes,
                y: sellValues,
                line: {color: '#FF0000'}
              }

              var data = [trace1,trace2];

              var layout = {
              title: ''+instrumentName+' Market Trades'
            };

             Plotly.newPlot('myDiv', data, layout);

             var trace1 = {
                type: "scatter",
                mode: "lines",
                name: 'Buy',
                x: buyTimes1,
                y: buyValues,
                line: {color: '#0000FF'}
              }

              var trace2 = {
                type: "scatter",
                mode: "lines",
                name: 'Sell',
                x: sellTimes1,
                y: sellValues,
                line: {color: '#FF0000'}
              }

              var data = [trace1,trace2];

              var layout = {
              title: ''+instrumentName+' Market Trades over Time'
            };

             Plotly.newPlot('myDiv1', data, layout);
      }

    },
    error: function(msg) {
      console.log("didnt work");
    }
    });

    });
 });
 