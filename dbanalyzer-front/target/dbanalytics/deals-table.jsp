<%
    if (((String)(session.getAttribute("shouldAllowAccess")))!="true"){
        out.print("Please, sign in before going here");
    }
    else{
%>
<style>

    .axis .domain {
        display: none;
    }

</style>
<script src="navControls.js"></script>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><img src="https://png.icons8.com/windows/50/ffffff/rocket.png" class="navbarIcon">Apollo 13</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item" id="navbarDeals">
                <a class="nav-link">Deals</a>
            </li>
            <li class="nav-item" id="navbarInstruments">
                <a class="nav-link" >Instruments</a>
            </li>
            <li class="nav-item" id="navbarCounterParty">
                <a class="nav-link" >Counter Party</a>
            </li>
            <li class="nav-item active" id="navbarDealsTable">
                <a class="nav-link" >Deals Table</a>
            </li>
        </ul>
    </div>
</nav>
<div class="row">
    <div class="col-md-2">
        &nbsp;
    </div>
    <div class="col-md-8">
        <h3 class="text-center cardTitles">Plain deals table</h3>
        <table id="plainDataTable" class="stripe text-center">
            <thead>
            <tr>
                <th>Id</th>
                <th>Amount</th>
                <th>Quantity</th>
                <th>Counterparty</th>
                <th>Instrument</th>
                <th>Type</th>
                <th>Time</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="col-md-2">

    </div>
</div>
<%--<script src="avgInstrumentPriceGraph.js"></script>--%>
<script>

</script>
<%--<script src="avgInstrumentPriceGraph.js"></script>--%>
<script>
    $('#plainDataTable').DataTable({
        "searching": false,
        "serverSide": true,
        ajax: {
            url: "plainDeals.jsp",
            type: "GET"
        }
    });
</script>
<%
    }
%>
